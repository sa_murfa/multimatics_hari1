package id.co.msmgroup.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity implements View.OnClickListener{

    private EditText kodePosInput;
    private EditText emailInput;
    private Button buttonInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        kodePosInput = (EditText) findViewById(R.id.kodePos);
        emailInput = (EditText) findViewById(R.id.emailPos);
        buttonInput = (Button) findViewById(R.id.inputButton);

        buttonInput.setOnClickListener(this);
        kodePosInput.setText("test");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.inputButton:
                Toast.makeText(getApplicationContext(), kodePosInput.getText(), Toast.LENGTH_LONG).show();
                break;
        }
//        getWilayah();
        getGeocode(emailInput.getText().toString());
    }

    private void getWilayah() {

        // Parsing Date
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        Retrofit.Builder builder = new Retrofit.Builder();
        Retrofit retrofit =  builder
                .baseUrl("http://www.lar11naja.com")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(new OkHttpClient.Builder().build())
                .build();

        WilayahApi wilayahApi = retrofit.create(WilayahApi.class);
        Observable<List<Wilayah>> hasil = wilayahApi.all();

        hasil.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<List<Wilayah>>() {
            @Override
            public void onNext(List<Wilayah> wilayahs) {
                for (Wilayah wilayah: wilayahs) {
                    Log.d("retrofit berhasil", wilayah.toString());
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e("retrofit error", e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void getGeocode(String address) {
        Retrofit.Builder builder = new Retrofit.Builder();
        Retrofit retrofit = builder
                .baseUrl("https://maps.googleapis.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(new OkHttpClient.Builder().build())
                .build();

        WilayahApi wilayahApi = retrofit.create(WilayahApi.class);
        Observable<GeocodeResponse> hasil = wilayahApi.geocode(address,"AIzaSyAVnOKDnZsM-qIjsZ37KNhb-iUnGxDigwo");

        hasil.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<GeocodeResponse>() {
            @Override
            public void onNext(GeocodeResponse geocodeResponse) {
                Log.d("Retrofit berhasil", geocodeResponse.getResults().get(0).getFormatted_address());
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Retrofit error", e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }
}
