package id.co.msmgroup.myapplication;

import java.util.List;
import java.util.Observable;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WilayahApi {

    @GET("/wilayah/list")
    public io.reactivex.Observable<List<Wilayah>> all();

    @GET("/maps/api/geocode/json?")
    public io.reactivex.Observable<GeocodeResponse> geocode(
            @Query("address") String address,
            @Query("key") String key
    );
}
