package id.co.msmgroup.myapplication;

import java.util.Date;

public class Wilayah {

    private int id_wilayah;
    private String nama;
    private int level;
    private int id_parent;
    private Date created_at;
    private Date updated_at;


    public int getId_wilayah() {
        return id_wilayah;
    }

    public void setId_wilayah(int id_wilayah) {
        this.id_wilayah = id_wilayah;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getId_parent() {
        return id_parent;
    }

    public void setId_parent(int id_parent) {
        this.id_parent = id_parent;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("nama: ");
        builder.append(getNama());
        builder.append(" ,id: ");
        builder.append(getId_wilayah());
        builder.append(" ,update: ");
        builder.append(updated_at);

        return builder.toString();
    }
}
